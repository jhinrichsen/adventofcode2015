package adventofcode2015

import "fmt"

func exampleFilename(day int) string {
	return fmt.Sprintf("testdata/day%d_example.txt", day)
}

func filename(day int) string {
	return fmt.Sprintf("testdata/day%d.txt", day)
}
